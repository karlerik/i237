#ifndef CLI_MICRORL_H
#define CLI_MICRORL_H
/* Commands execute callback for microrl */
int cli_execute(int argc, const char *const *argv);
#endif /* CLI_MICRORL_H */
