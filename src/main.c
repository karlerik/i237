#include <stdio.h>
#include <time.h>
#define __ASSERT_USE_STDERR
#include <assert.h>
#include <avr/io.h>
#include <util/delay.h>
#include "../lib/eriks_freemem/freemem.h"
#include "print_helper.h"
#include "hmi.h"
#include "avr/pgmspace.h"
#include <string.h>
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr_uart/uart.h"
#include "../lib/helius_microrl/microrl.h"
#include <avr/interrupt.h>
#include "cli_microrl.h"

#define UART_BAUD           9600
#define UART_STATUS_MASK    0x00FF
#define LED_RED         PORTA1
#define LED_GREEN       PORTA3
#define LED_BLUE        PORTA5

microrl_t rl;
microrl_t *prl = &rl;

static inline void init_con_uart(void)
{
    uart1_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart0_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart0_puts_p(PSTR("Console started \r\n"));
}

static inline void init_sys_timer(void)
{
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1B |= _BV(WGM12);
    TCCR1B |= _BV(CS12);
    OCR1A = 62549;
    TIMSK1 |= _BV(OCIE1A);
}

static inline void blocking_delay(void)
{
    PORTA ^= _BV(LED_RED);
    _delay_ms(BLINK_DELAY_MS);
}


static inline void print_sys_time(void)
{
    char iso_time[20] = {0x00};
    struct tm now_tm;
    time_t now = time(NULL);
    gmtime_r(&now, &now_tm);
    isotime_r(&now_tm, iso_time);
    uart1_puts(iso_time);
    uart1_puts_p(PSTR("\r\n"));
}


static inline void init_leds(void)
{
    DDRB |= _BV(DDB7);
    PORTB &= ~_BV(PORTB7);
    DDRA |= _BV(DDA1);
    DDRA |= _BV(DDA3);
    DDRA |= _BV(DDA5);
}

static inline void init_lcd(void)
{
    lcd_init();
    lcd_goto(LCD_ROW_1_START);
    lcd_puts_P(stud_name);
}

static inline void heartbeat(void)
{
    static time_t prev_time;
    char ascii_buf[11] = {0x00};
    time_t now = time(NULL);

    if (prev_time != now) {
        ltoa(now, ascii_buf, 10);
        uart1_puts_p(PSTR("Uptime: "));
        uart1_puts(ascii_buf);
        uart1_puts_p(PSTR(" s.\r\n"));
        PORTA ^= _BV(LED_GREEN);
        prev_time = now;
    }
}

void main(void)
{
    microrl_init(prl, uart0_puts);
    microrl_set_execute_callback(prl, cli_execute);
    init_leds();
    init_sys_timer();
    sei();
    init_con_uart();
    init_lcd();
    uart0_puts_p(motd);
    uart0_puts_p(PSTR("\r\n"));
    uart0_puts_p(stud_name);
    uart0_puts_p(PSTR("\r\n"));
    uart1_puts_p(ver_fw);
    uart1_puts_p(ver_libc);
    print_banner_P(uart0_puts_p, banner, BANNER_ROWS);

    while (1) {
        print_sys_time();
        heartbeat();
        microrl_insert_char(prl, (uart0_getc() & UART_STATUS_MASK));
    }
}

/* System timer ISR */
ISR(TIMER1_COMPA_vect)
{
    system_tick();
}
