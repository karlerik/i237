#include "hmi.h"
#include <avr/pgmspace.h>

#define VER_FW "Version: " FW_VERSION " built on: " __DATE__ " "__TIME__"\r\n"
#define VER_LIBC "avr-libc version: " __AVR_LIBC_VERSION_STRING__ " avr-gcc version: " __VERSION__ "\r\n"
const char ver_fw[] PROGMEM = VER_FW;
const char ver_libc[] PROGMEM = VER_LIBC;


const char stud_name[] PROGMEM = "Karl Erik Ounapuu";
const char motd[] PROGMEM = "Console started";
const char month_prompt[] PROGMEM = "Enter Month name first letter >";

static const char month1[] PROGMEM = "January";
static const char month2[] PROGMEM = "February";
static const char month3[] PROGMEM = "March";
static const char month4[] PROGMEM = "April";
static const char month5[] PROGMEM = "May";
static const char month6[] PROGMEM = "June";

PGM_P const name_month[] PROGMEM = {month1, month2, month3, month4, month5, month6};

const char b1[] PROGMEM =
    "\x1B[32m  __ \x1B[34m ____ \x1B[35m ____ \x1B[36m ____ ";
const char b2[] PROGMEM =
    "\x1B[32m (  )\x1B[34m(___ \\\x1B[35m( __ \\\x1B[36m(__  )";
const char b3[] PROGMEM =
    "\x1B[32m  )( \x1B[34m / __/ \x1B[35m(__ ( \x1B[36m / / ";
const char b4[] PROGMEM =
    "\x1B[32m (__)\x1B[34m(____)\x1B[35m(____/ \x1B[36m(_/\x1B[0m";

PGM_P const banner[] PROGMEM = {b1, b2, b3, b4};