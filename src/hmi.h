#include <avr/pgmspace.h>
#ifndef HMI_H
#define HMI_H

#define BLINK_DELAY_MS 500
#define LED_RED PORTA1 // Arduino Mega digital pin 23
#define LED_GREEN PORTA3 // Arduino Mega digital pin 25
#define LED_BLUE PORTA5 // Arduino Mega digital pin 27

#define NAME_MONTH_COUNT 6
#define BANNER_ROWS 4

extern const char stud_name[];
extern PGM_P const name_month[];
extern PGM_P const banner[] PROGMEM;
extern const char motd[];
extern const char month_prompt[];

extern const char ver_fw[] PROGMEM;
extern const char ver_libc[] PROGMEM;
#endif /* HMI_H */

